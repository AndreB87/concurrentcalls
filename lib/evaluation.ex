defmodule ConcurrentCalls.Evaluation do
  @moduledoc """
  Module to find the concurrent calls.
  """

  @spec evaluate([{map, non_neg_integer}]) :: map
  @doc """
  Evaluate the given calls to find concurrent ones.
  """
  def evaluate(calls) do
    evaluate(calls, %{})
  end

  defp evaluate([], result_map) do
    result_map
  end

  defp evaluate([call | rest], result_map) do
    evaluate(rest, evaluate_call(call, result_map))
  end

  defp evaluate_call({date_time, length}, result_map) do
    { seconds, _ } = DateTime.to_gregorian_seconds(date_time)
    evaluate_call(seconds, length, result_map)
  end

  defp evaluate_call(_date_time, 0, result_map) do
    result_map
  end

  defp evaluate_call(date_time, remaining_length, result_map) do
    case Map.fetch(result_map, date_time) do
      {:ok, concurrent_calls} ->
        evaluate_call(
          date_time + 1,
          remaining_length - 1,
          %{ result_map | date_time => concurrent_calls + 1 }
        )
      _else ->
        evaluate_call(
          date_time + 1,
          remaining_length - 1,
          Map.put_new(result_map, date_time, 1)
        )
    end
  end

end
