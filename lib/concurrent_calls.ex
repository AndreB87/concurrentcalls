defmodule ConcurrentCalls do
  @moduledoc """
  Implementation to the concurrent call challenge
  from https://gnulinux.ch/wettbewerb-parallele-anrufe.
  """

  alias ConcurrentCalls.Evaluation, as: Eval
  alias ConcurrentCalls.Parser, as: Parser


  @spec run :: map
  @doc """
  Entry point.
  """
  def run() do
    Parser.parse("resources/data.csv")
    |> concurrent_evaluation()
  end

  @doc """
  Entry point to test the application.
  """
  def run_test() do
    Parser.parse("resources/test_data.csv")
    |> concurrent_evaluation()
  end


  # Starts the evaluation of the calls in multiple nodes.
  defp concurrent_evaluation calls do
    call_lists = Enum.chunk_every(calls, 100)
    start_nodes(call_lists, self())
    reduce_node(%{}, Enum.count(call_lists))
  end


  # Starts a node to evaluate calls
  defp start_nodes [calls | []], return_pid do
    spawn(fn -> eval_node(calls, return_pid) end)
  end

  defp start_nodes [calls | remain], return_pid do
    spawn(fn -> eval_node(calls, return_pid) end)
    start_nodes(remain, return_pid)
  end


  # Returns the result to the given pid
  defp eval_node calls, return_pid do
    send(return_pid, {:ok, Eval.evaluate(calls)})
  end


  # Reduces the results from multiple nodes
  defp reduce_node result, 0 do
    result
    |> Map.values()
    |> Enum.frequencies()
  end

  defp reduce_node result, remaining_nodes do
    receive do
      {:ok, part_result} ->
        reduce_node(Map.merge(result, part_result, fn _k, v1, v2 -> v1 + v2 end), remaining_nodes - 1)
      any ->
        IO.inspect(any)
        reduce_node result, remaining_nodes
    end
  end

end
