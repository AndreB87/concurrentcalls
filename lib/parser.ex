defmodule ConcurrentCalls.Parser do

  def parse filename do
    File.stream!(filename)
    |> Stream.map(&String.trim(&1))
    |> Stream.map(&parse_line(&1))
    |> Enum.to_list()
  end

  defp parse_line line do
    { date_time, duration } = split_line(line)
    timestamp = parse_date_time(date_time)
    duration_in_seconds = parse_duration(duration)
    {timestamp, duration_in_seconds}
  end

  defp parse_date_time date_time do
    { date, time } = split_date_time(date_time)
    { day, month, year } = split_date(date)
    { hour, minute, second } = split_time(time)
    %DateTime{
      year: String.to_integer(year),
      month: String.to_integer(month),
      day: String.to_integer(day),
      hour: String.to_integer(hour),
      minute: String.to_integer(minute),
      second: String.to_integer(second),
      time_zone: "Europe/Berlin",
      zone_abbr: "CET",
      utc_offset: 0,
      std_offset: 0
    }
  end

  defp parse_duration(duration) do
    { hour_str, minute_str, second_str } = split_duration(duration)
    hours = String.to_integer(hour_str)
    minutes = String.to_integer(minute_str)
    seconds = String.to_integer(second_str)
    (((hours * 60) + minutes) * 60) + seconds
  end

  defp split_line line do
    case String.split(line, ",") do
      [date_time, duration] ->
        { date_time, duration }
      _any ->
        raise "Cannot parse line " <> to_string(line)
    end
  end

  defp split_date_time date_time do
    case String.split(date_time, " ") do
      [date, time] ->
        { date, time }
      _any ->
        raise "Cannot parse DateTime " <> to_string(date_time)
    end
  end

  defp split_date date do
    case String.split(date, ".") do
      [day, month, year] ->
        { day, month, year }
    end
  end

  defp split_time time do
    case String.split(time, ":") do
      [hour, minute, second] ->
        { hour, minute, second }
      _any ->
        raise "Cannot parse Time " <> to_string(time)
    end
  end

  defp split_duration duration do
    case String.split(duration, ":") do
      [hour_str, minute_str, second_str] ->
        { hour_str, minute_str, second_str }
      _any ->
        raise "Cannot parse duration " <> to_string(duration)
    end
  end

end
